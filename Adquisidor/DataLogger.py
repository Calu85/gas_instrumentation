#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtGui
import PyQt4.QtCore as core
import numpy as np
#~ import serial
import fakeSerial as serial
from adq_widget import Ui_ADQ_widget
from pint import UnitRegistry
import pyqtgraph as pg
unit=UnitRegistry()
import pdb

class Fisica:
    """Contiene las variables que representan cantidades físicas y las funciones que las modifican"""
    unidadEntrada=unit.kPa
    unidadSalida=unit.kPa
    factor_conversion=np.zeros(3) * unidadEntrada
    Pmedida = np.zeros(3) * unidadSalida

    #~ def setUnidadEntrada(self, unidad_elegida):  
      #~ self.unidadEntrada=unidad_elegida
             
    def calibrar(self, Patm, lectura_AD):
      """Calculo del factor de conversión según la presión atmosférica del momento"""
      self.factor_conversion = Patm/lectura_AD

    def calcularPresion(self, lectura_AD):
      """Convierto el valor numérico del ADC a presión, según el factor de conversión de cada canal"""
      self.Pmedida = self.factor_conversion * lectura_AD
      return self.Pmedida
      
    def setUnidadSalida(self, unidad_elegida):
      """Convierte Pmedida a la unidad elegida desde la GUI"""
      self.unidadSalida=unidad_elegida
      self.factor_conversion=self.factor_conversion.to(unidad_elegida)

class GUI(QtGui.QWidget):
    """ Interfaz gráfica"""
    fisica = Fisica()
    
    #~ Creo el objeto serial que se comunica con el puerto
    ser = serial.Serial('/dev/ttyACM0', 9600)
    #~ ser.flushInput()
    #~ ser.flushOutput()
    #~ data_raw va a tener un string con los tres valores separados por coma, porque así vienen por el puerto
    data_raw = ser.readline()
    #~ Estas variables son para la grabación en archivo. Cacho es el buffer que graba.
    cacho = np.zeros((10,3))
    i=0
    grabando = False
    #~ Este es el buffer para graficar.
    graphBuffer = np.zeros((100,3))
    
    def __init__(self):
        super(GUI, self).__init__()
        #QtGui.QMainWindow.__init__(self)      
        self.ui = Ui_ADQ_widget()
        self.ui.setupUi(self)          
        #~ Defino las acciones, calibrar y grabar.              
        self.ui.boton_calibrar.clicked.connect(self.apriete)
        self.ui.boton_grabar.clicked.connect(self.grabar)
        
        #~ lr es la región entre ambas lineas, sirve para medir la distancia.       
        def updateRegion():
          print(lr.getRegion())
        
        self.ui.canvas.setBackground('#E6D6BB')
        lr = pg.LinearRegionItem([1, 30], brush='#615541', bounds=[0,100], movable=True)
        self.ui.canvas.addItem(lr)
        #~ line = pg.InfiniteLine(angle=90, movable=True)
        #~ self.ui.canvas.addItem(line)
        #~ line.setBounds([0,200])
        lr.sigRegionChanged.connect(updateRegion)
        #~ Para mostrar cómo es nada más, muestro los límites de la región, no sirve esto
        print(lr.getRegion())
        self.show()
        
    def parseSerialInput(self, linea):
        """Lee el dato que ingresa por puerto serie y lo separa en los tres valores"""
        vector = np.array (np.fromstring(linea, sep=';'))
        return vector
    
    def updateGrafica(self, ultimoValor):
        """ Actualizo los valores de la gráfica para que se desplace hacia atrás"""
        #~ Defino ventana_grafica para simplificar la escritura nada más.
        ventana_grafica = self.ui.canvas
        #~ Con roll desplazo cada elemento de graphBuffer en un lugar hacia atrás (-1), a lo largo del primer eje (0).
        self.graphBuffer=np.roll(self.graphBuffer, -1, 0)
        ultimoValor[1]=ultimoValor[1]-10*unit.kPa
        ultimoValor[2]=ultimoValor[2]-20*unit.kPa
        #~ El último elemenot, ahora que hice roll, es el que era el primero antes, y es el que reemplazo por el valor recién leído.
        #~ Con estas dos cosas, construyo una estructura tipo fifo.
        self.graphBuffer[self.graphBuffer.shape[0]-1]=ultimoValor
        #~ Agrego las curvas. el clear=True es para que no acumule, borra lo anterior en cada actualización
        ventana_grafica.plot(self.graphBuffer[:,0], pen=(255,0,0), clear=True)
        ventana_grafica.plot(self.graphBuffer[:,1], pen=(0,255,0))
        ventana_grafica.plot(self.graphBuffer[:,2], pen=(0,0,255))
        
    def apriete(self):
        """Calibra el sistema con la presión atmosférica"""
        # Compruebo que el valor ingresado es un número
        if self.is_number(self.ui.edit_Patm.text()):
            # Si lo es, convierto el string a float
            Patm = float((self.ui.edit_Patm.text())) * unit.parse_expression(str(self.ui.combo_unidades.currentText()))
            # Obtengo el último valor del AD, primero como string y luego como float
            lectura_serial= self.ser.readline()
            vector_numerico = self.parseSerialInput(lectura_serial)
            # Llamo a calibrar para falcular el factor de conversion
            self.fisica.calibrar(Patm, vector_numerico)
            vector_presiones=self.fisica.calcularPresion(vector_numerico)
            self.ui.l_indicador_calibrado.setText(u"Sistema calibrado")
          
    def is_number(self, s):
        """Se fija si el valor ingresado es un float"""
        try:
          float(s)
          return True
        except ValueError:
          return False
    
    def refrescar(self):
      """Actualiza los valores de presión de cada sensor"""
      data_raw = self.ser.readline()
      vector_numerico = self.parseSerialInput(data_raw)
      lastPressure = self.fisica.calcularPresion(vector_numerico)
      # dentro de los corchetes, se reemplazan los : por lo que se ponga después, en format.
      # El ~ sirve para indicarle a pint que formate unidad en la versión corta, km en lugar de kilometer
      self.ui.l_indicador_s1.setText('{:~}'.format(lastPressure[0]))
      self.ui.l_indicador_s2.setText('{:~}'.format(lastPressure[1]))
      self.ui.l_indicador_s3.setText('{:~}'.format(lastPressure[2]))
      if (self.grabando):
        self.cacho[self.i]=lastPressure
        self.i += 1
        print(self.i)
        if (self.i == 10):
          np.savetxt("foo.csv", self.cacho, delimiter=",", header="Poner header")
          self.i = 0
          self.cacho = np.zeros((10,3))
          print(self.i)
      self.updateGrafica(lastPressure)
          
    def grabar(self):
      """Graba a archivo los datos adquiridos
        Cambia el estado del botón cada vez que se comienza o termina una grabación, y el texto que posee."""
      if not(self.ui.boton_grabar.isChecked()):
        self.ui.boton_grabar.setText(u'Comenzar grabación')
        self.grabando=False
      if self.ui.boton_grabar.isChecked():
        self.ui.boton_grabar.setText(u'Detener grabación')
        self.grabando=True
        
def main():
    #~ Fijo el período de actualización de los datos.
    refreshMillis = 1000
    app = QtGui.QApplication(sys.argv)
    ex = GUI()
    timer = core.QTimer()
    timer.timeout.connect(ex.refrescar)
    timer.start(refreshMillis)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
