# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GUI_adq/adq_widget.ui'
#
# Created: Mon Apr 13 15:41:26 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ADQ_widget(object):
    def setupUi(self, ADQ_widget):
        ADQ_widget.setObjectName(_fromUtf8("ADQ_widget"))
        ADQ_widget.resize(689, 610)
        self.frame = QtGui.QFrame(ADQ_widget)
        self.frame.setGeometry(QtCore.QRect(300, 10, 311, 201))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.l_indicador_calibrado = QtGui.QLabel(self.frame)
        self.l_indicador_calibrado.setGeometry(QtCore.QRect(80, 100, 161, 20))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.l_indicador_calibrado.setFont(font)
        self.l_indicador_calibrado.setObjectName(_fromUtf8("l_indicador_calibrado"))
        self.edit_Patm = QtGui.QLineEdit(self.frame)
        self.edit_Patm.setGeometry(QtCore.QRect(120, 60, 111, 31))
        self.edit_Patm.setObjectName(_fromUtf8("edit_Patm"))
        self.combo_unidades = QtGui.QComboBox(self.frame)
        self.combo_unidades.setGeometry(QtCore.QRect(240, 60, 61, 31))
        self.combo_unidades.setObjectName(_fromUtf8("combo_unidades"))
        self.combo_unidades.addItem(_fromUtf8(""))
        self.combo_unidades.addItem(_fromUtf8(""))
        self.combo_unidades.addItem(_fromUtf8(""))
        self.combo_unidades.addItem(_fromUtf8(""))
        self.l_Patm = QtGui.QLabel(self.frame)
        self.l_Patm.setGeometry(QtCore.QRect(120, 40, 161, 16))
        self.l_Patm.setObjectName(_fromUtf8("l_Patm"))
        self.boton_calibrar = QtGui.QPushButton(self.frame)
        self.boton_calibrar.setGeometry(QtCore.QRect(30, 60, 81, 31))
        self.boton_calibrar.setObjectName(_fromUtf8("boton_calibrar"))
        self.layoutWidget = QtGui.QWidget(ADQ_widget)
        self.layoutWidget.setGeometry(QtCore.QRect(110, 72, 181, 131))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.l_indicador_s1 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.l_indicador_s1.setFont(font)
        self.l_indicador_s1.setObjectName(_fromUtf8("l_indicador_s1"))
        self.verticalLayout.addWidget(self.l_indicador_s1)
        self.l_indicador_s2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.l_indicador_s2.setFont(font)
        self.l_indicador_s2.setObjectName(_fromUtf8("l_indicador_s2"))
        self.verticalLayout.addWidget(self.l_indicador_s2)
        self.l_indicador_s3 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.l_indicador_s3.setFont(font)
        self.l_indicador_s3.setObjectName(_fromUtf8("l_indicador_s3"))
        self.verticalLayout.addWidget(self.l_indicador_s3)
        self.layoutWidget1 = QtGui.QWidget(ADQ_widget)
        self.layoutWidget1.setGeometry(QtCore.QRect(0, 70, 101, 131))
        self.layoutWidget1.setObjectName(_fromUtf8("layoutWidget1"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.layoutWidget1)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.l_s1 = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.l_s1.setFont(font)
        self.l_s1.setObjectName(_fromUtf8("l_s1"))
        self.verticalLayout_2.addWidget(self.l_s1)
        self.l_s2 = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.l_s2.setFont(font)
        self.l_s2.setObjectName(_fromUtf8("l_s2"))
        self.verticalLayout_2.addWidget(self.l_s2)
        self.l_s3 = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.l_s3.setFont(font)
        self.l_s3.setObjectName(_fromUtf8("l_s3"))
        self.verticalLayout_2.addWidget(self.l_s3)
        self.combo_unidades_2 = QtGui.QComboBox(ADQ_widget)
        self.combo_unidades_2.setGeometry(QtCore.QRect(200, 20, 61, 31))
        self.combo_unidades_2.setObjectName(_fromUtf8("combo_unidades_2"))
        self.combo_unidades_2.addItem(_fromUtf8(""))
        self.combo_unidades_2.addItem(_fromUtf8(""))
        self.combo_unidades_2.addItem(_fromUtf8(""))
        self.combo_unidades_2.addItem(_fromUtf8(""))
        self.canvas = PlotWidget(ADQ_widget)
        self.canvas.setGeometry(QtCore.QRect(30, 320, 611, 271))
        self.canvas.setObjectName(_fromUtf8("canvas"))
        self.boton_grabar = QtGui.QPushButton(ADQ_widget)
        self.boton_grabar.setGeometry(QtCore.QRect(20, 240, 141, 31))
        self.boton_grabar.setObjectName(_fromUtf8("boton_grabar"))

        self.retranslateUi(ADQ_widget)
        QtCore.QMetaObject.connectSlotsByName(ADQ_widget)

    def retranslateUi(self, ADQ_widget):
        ADQ_widget.setWindowTitle(_translate("ADQ_widget", "ADQ_widget", None))
        self.l_indicador_calibrado.setText(_translate("ADQ_widget", "Sistema sin calibrar", None))
        self.combo_unidades.setItemText(0, _translate("ADQ_widget", "kPa", None))
        self.combo_unidades.setItemText(1, _translate("ADQ_widget", "Pa", None))
        self.combo_unidades.setItemText(2, _translate("ADQ_widget", "mbar", None))
        self.combo_unidades.setItemText(3, _translate("ADQ_widget", "atm", None))
        self.l_Patm.setText(_translate("ADQ_widget", "Presión atmosférica actual", None))
        self.boton_calibrar.setText(_translate("ADQ_widget", "Calibrar", None))
        self.l_indicador_s1.setText(_translate("ADQ_widget", "Sin calibrar", None))
        self.l_indicador_s2.setText(_translate("ADQ_widget", "Sin calibrar", None))
        self.l_indicador_s3.setText(_translate("ADQ_widget", "Sin calibrar", None))
        self.l_s1.setText(_translate("ADQ_widget", "Sensor 1", None))
        self.l_s2.setText(_translate("ADQ_widget", "Sensor 2", None))
        self.l_s3.setText(_translate("ADQ_widget", "Sensor 3", None))
        self.combo_unidades_2.setItemText(0, _translate("ADQ_widget", "kPa", None))
        self.combo_unidades_2.setItemText(1, _translate("ADQ_widget", "Pa", None))
        self.combo_unidades_2.setItemText(2, _translate("ADQ_widget", "mbar", None))
        self.combo_unidades_2.setItemText(3, _translate("ADQ_widget", "atm", None))
        self.boton_grabar.setText(_translate("ADQ_widget", "Comenzar a grabar", None))

from pyqtgraph import PlotWidget
