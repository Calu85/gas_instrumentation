#ifndef ADQ_WIDGET_H
#define ADQ_WIDGET_H

#include <QWidget>

namespace Ui {
class ADQ_widget;
}

class ADQ_widget : public QWidget
{
    Q_OBJECT

public:
    explicit ADQ_widget(QWidget *parent = 0);
    ~ADQ_widget();

private:
    Ui::ADQ_widget *ui;
};

#endif // ADQ_WIDGET_H
