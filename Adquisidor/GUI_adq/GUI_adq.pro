#-------------------------------------------------
#
# Project created by QtCreator 2015-02-18T15:58:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI_adq
TEMPLATE = app


SOURCES += main.cpp\
        adq_widget.cpp

HEADERS  += adq_widget.h

FORMS    += adq_widget.ui
