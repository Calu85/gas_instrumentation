.. GAS_adq documentation master file, created by
   sphinx-quickstart on Fri Mar 13 08:37:44 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GAS_adq's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: DataLogger

.. autoclass:: Fisica
    :members:
    
.. autoclass:: GUI
    :members:

.. inheritance-diagram:: DataLogger
   :parts: 1

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

