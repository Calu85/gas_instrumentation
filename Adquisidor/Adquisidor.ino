/*Código para adquisión de los sensores de presión.
  Realizo la adquisición de tres canales, y luego la conversión a valores de presión.
*/

int sensor_input = A0; // Sensor en la entrada de la tobera. A0 es un alias ya definido en arduino.
int sensor_out1 = A1; //Sensor en la salida 1 de la tobera
int sensor_out2 = A2; // Sensor en la salida 2 de la tobera

int value_input = 0;  // Almacena el valor entero luego del ADC, para sensor en entrada
int value_out1 = 0;  // Almacena el valor entero luego del ADC, para sensor en salida 1
int value_out2 = 0;  // Almacena el valor entero luego del ADC, para sensor en salida 2


void setup() {
  //analogReference(DEFAULT); // Esto sea el AD a 3.3 V, pero en Arduino DUE esta función no se aplica, el micro la ignora.
  analogReadResolution(12); // Seteo los 12 bits que permite el Arduino Due
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  Serial.begin(9600); 
}

void loop() {
  // Leo los valores analógicos y convierto
  value_input = analogRead(sensor_input);    
  value_out1 = analogRead(sensor_out1);    
  value_out2 = analogRead(sensor_out2);    
  
	Serial.print(value_input);
	Serial.print(';');
	Serial.print(value_out1);
	Serial.print(';');
	Serial.print(value_out2);
	Serial.print("\n"); 
  
}


